#include "SoftwareRasteriser.h"
#include "RenderObject.h"
#include "Mesh.h"
#include "Texture.h"

int main() {
	SoftwareRasteriser r(800,600);

		RenderObject* stars[1000];

	for (int i = 0; i < 1000; i++) {
		stars[i] = new RenderObject;
		stars[i]->mesh = Mesh::GenerateStar(); //store 1000 stars into the array
	}

		RenderObject * strip = new RenderObject();
		strip->mesh = Mesh::GenerateStrip();
		strip->modelMatrix = Matrix4::Translation(Vector3(3, 2,-10));

		RenderObject * cube = new RenderObject();
		cube->mesh = Mesh::LoadMeshFile("../cube.asciimesh");
		cube->modelMatrix = Matrix4::Translation(Vector3(2, 0, -10));

		RenderObject * o1 = new RenderObject();
		o1 -> mesh = Mesh::GenerateTriangle();
		o1 -> modelMatrix = Matrix4::Translation(Vector3(0, 0, -7));
		o1 -> texture = Texture::TextureFromTGA("../brick.tga");
		
		RenderObject * o2 = new RenderObject();
		o2 -> mesh = Mesh::GenerateTriangle();
		o2 -> modelMatrix = Matrix4::Translation(Vector3(0, 0, -10));
		o2 -> texture = Texture::TextureFromTGA("../brick.tga");

		RenderObject * fan = new RenderObject();
		fan -> mesh = Mesh::GenerateFan();
		fan->modelMatrix = Matrix4::Translation(Vector3(-3, 0, -10));

		RenderObject* ship = new RenderObject();
		ship->mesh = Mesh::GenerateShip();
		ship->modelMatrix = Matrix4::Translation(Vector3(0, 0, -5));
		
		RenderObject * line = new RenderObject(); // create a line mesh and move it like a asteroid
		line->mesh = Mesh::GenerateLine(Vector3(-0.20f, -0.20f, -2), Vector3(0.5f, 0.5f, -2)); // make it grey to white
		line->modelMatrix = Matrix4::Translation(Vector3(6, 6, -10));

		
		

		 Matrix4 viewMatrix;

		 float yaw = 0.0f;
		 float aspect = 800.0f / 600.0f;
	     while(r.UpdateWindow()) {
		yaw += Mouse::GetRelativePosition().x;
	
	//	viewMatrix = viewMatrix * Matrix4::Rotation(yaw, Vector3(0, 1, 0)); // rotate according to the mouse
		
		r.ClearBuffers();

		r.SetProjectionMatrix(Matrix4::Perspective(1.0f, 100.0f, aspect, 45.0f));

		r.DrawObject(o1);
	    r.DrawObject(o2);
		for (size_t i = 0; i < 1000; i++)
		{
			r.DrawObject(stars[i]);
		}
		r.DrawObject(ship); 
		r.DrawObject(cube);
		r.DrawObject(fan);
		r.DrawObject(strip);

		
		cube->modelMatrix = cube->modelMatrix * Matrix4::Rotation(3, Vector3(3, 4, 20)); //adding translation after the objejet will create a moving effect
		
		

		if (Mouse::ButtonDown(MOUSE_LEFT)) {
			std::cout << "Mouse is at position: " << Mouse::GetAbsolutePosition().x << " , " << Mouse::GetAbsolutePosition().y << std::endl;
		}

		if (Keyboard::KeyDown(KEY_A)) {   //press wasd can move the objects
			 viewMatrix = viewMatrix *
				 Matrix4::Translation(Vector3(-0.030f, 0, 0));
			
		}
		if (Keyboard::KeyDown(KEY_D)) {
			viewMatrix = viewMatrix *
				Matrix4::Translation(Vector3(0.030f, 0, 0));
		}
		
		if (Keyboard::KeyDown(KEY_W)) {
				  viewMatrix = viewMatrix *
					  Matrix4::Translation(Vector3(0.0, 0.030f, 0));				 
		}
		
		if (Keyboard::KeyDown(KEY_S)) {
				 viewMatrix = viewMatrix *
					  Matrix4::Translation(Vector3(0.0, -0.030f, 0));		 
		} 

		if (Keyboard::KeyDown(KEY_J)){
				  ship->modelMatrix = ship->modelMatrix * Matrix4::Translation(Vector3(-0.01f, 0, 0));
		}

		if (Keyboard::KeyDown(KEY_K)) {
			ship->modelMatrix = ship->modelMatrix * Matrix4::Translation(Vector3(0, -0.01f, 0));
		}

		if (Keyboard::KeyDown(KEY_I)) {
			ship->modelMatrix = ship->modelMatrix * Matrix4::Translation(Vector3(0, 0.01f, 0));
		}

		if (Keyboard::KeyDown(KEY_L)) {
			ship->modelMatrix = ship->modelMatrix * Matrix4::Translation(Vector3(0.01f,0, 0));
		}
		if (Keyboard::KeyDown(KEY_M)) { //flying asteroid
			
			r.DrawObject(line);
			line->modelMatrix = line->modelMatrix * Matrix4::Translation(Vector3(-6, -6, -2));
			

		}

		// add keys to expand the size?

		r.SetViewMatrix(viewMatrix);
		r.SwapBuffers();

	}
		 delete line->mesh;
		 delete line;

		 delete fan->mesh;
		 delete fan;

		 delete o1->mesh;
		 delete o1;

		 delete o2->mesh;
		 delete o2;

		 delete ship->mesh;
		 delete ship;

		 delete cube->mesh;
		 delete cube;

		 delete strip->mesh;
		 delete strip;

		 for (int i = 0; i < 1000; i++) //deleting stars
		 {
			 delete stars[i]->mesh;
			 delete stars[i];
		 }
	//delete object->mesh; //these objects has to be delected, e.g. delete o1,o2 etc for memory leak 
	//delete object;

	return 0;
}