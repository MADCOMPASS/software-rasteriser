/******************************************************************************
Class:SoftwareRasteriser
Implements:Window
Author:Rich Davison	<richard.davison4@newcastle.ac.uk>
Description: Class to encapsulate the various rasterisation techniques looked
at in the course material.

This is the class you'll be modifying the most!

-_-_-_-_-_-_-_,------,
_-_-_-_-_-_-_-|   /\_/\   NYANYANYAN
-_-_-_-_-_-_-~|__( ^ .^) /
_-_-_-_-_-_-_-""  ""

*//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Matrix4.h"
#include "Mesh.h"
#include "Texture.h"
#include "RenderObject.h"
#include "Common.h"
#include "Window.h"

#include <vector>

using std::vector;

struct BoundingBox {
	Vector2 topLeft;
	Vector2 bottomRight;
};

enum SampleState {
	SAMPLE_NEAREST,
	SAMPLE_BILINEAR,
	SAMPLE_MIPMAP_NEAREST,
	SAMPLE_MIPMAP_BILINEAR // For you to implement !
	
};

class RenderObject;
class Texture;

class SoftwareRasteriser : public Window {
public:
	SoftwareRasteriser(uint width, uint height);
	~SoftwareRasteriser(void);

	void	DrawObject(RenderObject*o);

	void	ClearBuffers();
	void	SwapBuffers();

	inline void	SetViewMatrix(const Matrix4 &m) {
		viewMatrix = m;
		viewProjMatrix = projectionMatrix * viewMatrix;
	}

	void	SetProjectionMatrix(const Matrix4 &m) {
		projectionMatrix = m;
		viewProjMatrix = projectionMatrix * viewMatrix;
	}

	static float ScreenAreaOfTri(const Vector4 & v0,
		const Vector4 & v1,
		const Vector4 & v2);











protected:
	Colour*	GetCurrentBuffer();
	BoundingBox CalculateBoxForTri(const Vector4 &av0, const Vector4 &v1, const Vector4 &v2);   //const= constant vector4
	void	RasterisePointsMesh(RenderObject*o);
	void	RasteriseLinesMesh(RenderObject*o);

	void	RasteriseFanMesh(RenderObject*o);
	void	RasteriseStripMesh(RenderObject*o);

	virtual void Resize();

	void	RasteriseLine(const Vector4 &v0, const Vector4 &v1,
		const Colour &colA = Colour(255, 255, 255, 255), const Colour &colB = Colour(255, 255, 255, 255),
		const Vector2 &texA = Vector2(0, 0), const Vector2 &texB = Vector2(1, 1));

	inline bool DepthFunc(int x, int y, float depthValue) {
		if (x >= 0 && x < screenWidth
			&& y >= 0 && y < screenHeight)
		{
			int index = (y * screenWidth) + x;

			unsigned int castVal = (unsigned int)depthValue;

			if (castVal > depthBuffer[index]) {
				return false;

			}
			depthBuffer[index] = castVal;
			return true;
		}
		return false;
	}

	inline void BlendPixel(int x, int y, const Colour & source) {
		if (y >= screenHeight || y < 0) {
			return;

		}
		if (x >= screenWidth || x < 0) {
			return;

		}

		int index = (y * screenWidth) + x;

		Colour & dest = buffers[currentDrawBuffer][index];
		unsigned char sFactor = source.a;
		unsigned char dFactor = (255 - source.a);

		dest.r = ((source.r * sFactor) + (dest.r * dFactor)) / 255;
		dest.g = ((source.g * sFactor) + (dest.g * dFactor)) / 255;
		dest.b = ((source.b * sFactor) + (dest.b * dFactor)) / 255;
		dest.a = ((source.a * sFactor) + (dest.a * dFactor)) / 255;

	}

	inline void	ShadePixel(uint x, uint y, const Colour&c) {
		if (y >= screenHeight) {
			return;
		}
		if (x >= screenWidth) {
			return;
		}

		int index = (y * screenWidth) + x;

		buffers[currentDrawBuffer][index] = c;
	}


	void	RasteriseTriMesh(RenderObject*o);

	void	RasteriseTri(const Vector4 &v0, const Vector4 &v1, const Vector4 &v2,
		const Colour &c0 = Colour(), const Colour &c1 = Colour(), const Colour &c2 = Colour(),
		const Vector3 &t0 = Vector3(), const Vector3 &t1 = Vector3(), const Vector3 &t2 = Vector3());

	int		currentDrawBuffer;
	Texture* currentTexture;

	Colour*	buffers[2];

	unsigned short*	depthBuffer;


	Matrix4 viewMatrix;
	Matrix4 projectionMatrix;
	Matrix4 textureMatrix;

	Matrix4	viewProjMatrix;

	Matrix4	portMatrix;
};

