#include "Mesh.h"

Mesh::Mesh(void)	{
	type			= PRIMITIVE_POINTS;

	numVertices		= 0;

	vertices		= NULL;
	colours			= NULL;
	textureCoords	= NULL;
}

Mesh::~Mesh(void)	{
	delete[] vertices;
	delete[] colours;
	delete[] textureCoords;
}




Mesh*Mesh::GenerateLine(const Vector3 &from, const Vector3 &to) {




	Mesh* m = new Mesh();

	m->numVertices = 2;
	


	m->vertices = new Vector4[m->numVertices];
	m->colours = new Colour[m->numVertices];

	m->vertices[0] = Vector4(from.x, from.y, from.z, 1.0f);
	m->vertices[1] = Vector4(to.x, to.y, to.z, 1.0f);
	

	m->colours[0] = Colour(255, 255, 255, 255);
	m->colours[1] = Colour(255, 255, 255, 255);

	m->type = PRIMITIVE_LINES;

	return m;




}

Mesh* Mesh::GenerateTriangle() {

	Mesh*m = new Mesh();
	//m->type = PRIMITIVE_TRIANGLES;
	m->numVertices = 3; //3 vertices

	m -> vertices = new Vector4[m->numVertices];
	m -> colours = new Colour[m -> numVertices];
	m->textureCoords = new Vector2[m -> numVertices];

	m->vertices[0] = Vector4(0.5f, -0.5f, 0, 1.0f);
	m->vertices[1] = Vector4(0.0f, 0.5f, 0, 1.0f);
	m->vertices[2] = Vector4(-0.5f, -0.5f, 0, 1.0f);

	m -> colours[0] = Colour(255, 0, 0, 255); // Red - New line !
	m -> colours[1] = Colour(0, 255, 0, 255); // Green - New line !
	m -> colours[2] = Colour(0, 0, 255, 255); // BLue - New line !
	
	m -> textureCoords[0] = Vector2(0.0f, 0.0f);
	m -> textureCoords[1] = Vector2(0.5f, 1.0f);
	m -> textureCoords[2] = Vector2(1.0f, 0.0f);

	m->type = PRIMITIVE_TRIANGLES;

	return m;

}

Mesh* Mesh::GenerateStar() {

	Mesh *m = new Mesh();

	m->numVertices = 1; // number of vertice

	m->vertices = new Vector4[m->numVertices]; // one vertice
	m->colours = new Colour[m->numVertices]; // one colour

	float x = rand() % 200 - 100;

	float y = rand() % 200 - 100;
	
	float z = rand() % 200 - 100;
	
	m->vertices[0] = Vector4(x, y, z, 1.0f); // assigning the coordinates 
	m->colours[0] = Colour(255, 255, 255, 255); // making each vertice white

	m->type = PRIMITIVE_POINTS;

	return m;
}

Mesh *Mesh::GenerateShip() {

	Mesh*m = new Mesh();

	m->type = PRIMITIVE_TRIANGLES;

	m->numVertices = 24;

	m->vertices = new Vector4[m->numVertices];
	m->colours = new Colour[m->numVertices];
	m->textureCoords = new Vector2[m->numVertices];

	
	m->vertices[0] = Vector4(1.5f, 0, 0, 1.0f); // side face of the ship
	m->vertices[1] = Vector4(0, 0.5f, 0, 1.0f);
	m->vertices[2] = Vector4(0, 0, 0, 1.0f);

	m->vertices[3] = Vector4(1.5f, 0, 0, 1.0f); // top side of the ship
	m->vertices[4] = Vector4(1.5f, 0, -1, 1.0f);
	m->vertices[5] = Vector4(0, 0.5f, 0, 1.0f);

	m->vertices[6] = Vector4(1.5f, 0, -1, 1.0f);
	m->vertices[7] = Vector4(0, 0.5f, -1, 1.0f);
	m->vertices[8] = Vector4(0, 0.5f, 0, 1.0f);

	m->vertices[9] = Vector4(1.5f, 0, -1, 1.0f); // other side of the ship
	m->vertices[10] = Vector4(0, 0.5f, -1, 1.0f);
	m->vertices[11] = Vector4(0, 0, -1, 1.0f);

	m->vertices[12] = Vector4(0, 0, 0, 1.0f); // back of the ship
	m->vertices[13] = Vector4(0, 0.5f, 0, 1.0f);
	m->vertices[14] = Vector4(0, 0, -1, 1.0f);

	m->vertices[15] = Vector4(0, 0, -1, 1.0f);
	m->vertices[16] = Vector4(0, 0.5, 0, 1.0f);
	m->vertices[17] = Vector4(0, 0.5, -1, 1.0f);

	m->vertices[18] = Vector4(0, 0, 0, 1.0f); // bottom of the ship
	m->vertices[19] = Vector4(1.5, 0, 0, 1.0f);
	m->vertices[20] = Vector4(0, 0, -1, 1.0f);

	m->vertices[21] = Vector4(0, 0, -1, 1.0f);
	m->vertices[22] = Vector4(1.5, 0, 0, 1.0f);
	m->vertices[23] = Vector4(1.5, 0, -1, 1.0f);


	for (int i = 0; i < m->numVertices; i++) {
		m->colours[i] = Colour(255, 255, 255, 80);
	}

	m->colours[3] = Colour(211, 211, 211, 255);
	m->colours[4] = Colour(211, 211, 211, 255);
	m->colours[5] = Colour(211, 211, 211, 255);
	m->colours[6] = Colour(211, 211, 211, 255);
	m->colours[7] = Colour(211, 211, 211, 255);
	m->colours[8] = Colour(211, 211, 211, 255);
	m->colours[12] = Colour(211, 211, 211, 255);
	m->colours[13] = Colour(211, 211, 211, 255);
	m->colours[14] = Colour(211, 211, 211, 255);
	m->colours[15] = Colour(211, 211, 211, 255);
	m->colours[16] = Colour(211, 211, 211, 255);
	m->colours[17] = Colour(211, 211, 211, 255);


	return m;
}



Mesh *Mesh::GenerateStrip() {
	Mesh *m = new Mesh();

	m->numVertices = 6;

	m->vertices = new Vector4[m->numVertices];
	m->colours = new Colour[m->numVertices];
	m->textureCoords = new Vector2[m->numVertices];

	m->vertices[0] = Vector4(0, 0, 0, 1.0f);
	m->vertices[1] = Vector4(1, 1, 0, 1.0f);
	m->vertices[2] = Vector4(1, 0, 0, 1.0f);
	m->vertices[3] = Vector4(0, 0, 0, 1.0f);
	m->vertices[4] = Vector4(1, 0, 0, 1.0f);
	m->vertices[5] = Vector4(0, 1, 0, 1.0f);

	for (int i = 0; i < m->numVertices; i++) {
		m->colours[i] = Colour(255, 255, 255, 255);
	}


	m->type = PRIMITIVE_TRIANGLE_STRIP;

	return m;

}

Mesh *Mesh::GenerateFan() {
	Mesh *m = new Mesh();

	m->numVertices = 6;

	m->vertices = new Vector4[m->numVertices];
	m->colours = new Colour[m->numVertices];
	m->textureCoords = new Vector2[m->numVertices];

	m->vertices[0] = Vector4(0, 0, 0, 1.0f);
	m->vertices[1] = Vector4(-1, 0, 0, 1.0f);
	m->vertices[2] = Vector4(-1, -1, 0, 1.0f);

	m->vertices[3] = Vector4(0, -1, 0, 1.0f);
	m->vertices[4] = Vector4(1, -1, 0, 1.0f);
	m->vertices[5] = Vector4(1, 0, 0, 1.0f);


	m->colours[0] = Colour(255, 0, 0, 255);
	m->colours[1] = Colour(255, 255, 0, 255);
	m->colours[2] = Colour(255, 255, 255, 255);

	m->colours[4] = Colour(255, 255, 255, 255);
	m->colours[5] = Colour(255, 255, 0, 255);
	m->colours[3] = Colour(255, 0, 0, 255);


	m->type = PRIMITIVE_TRIANGLE_FAN;

	return m;

}

Mesh * Mesh::LoadMeshFile(const string & filename) {
	 ifstream f(filename);
	
		 if (!f) {
		 return NULL;
		
	}
	
		 Mesh * m = new Mesh();
	 m -> type = PRIMITIVE_TRIANGLES;
	 f >> m -> numVertices;
	
		 int hasTex = 0;
	 int hasColour = 0;
	
		f >> hasTex; // Oooh ! Sneak preview of later tutorial ...
	 f >> hasColour;
	
	 m -> vertices = new Vector4[m -> numVertices];
	 m -> textureCoords = new Vector2[m -> numVertices];
	 m -> colours = new Colour[m -> numVertices];
	
		 for (int i = 0; i < m -> numVertices; ++i) {
		 f >> m -> vertices[i].x;
		 f >> m -> vertices[i].y;
		 f >> m -> vertices[i].z;
		
	}
	 if (hasColour) {
		 for (int i = 0; i < m -> numVertices; ++i) {
			 f >> m -> colours[i].r; f >> m -> colours[i].g;
			 f >> m -> colours[i].b; f >> m -> colours[i].a;
			
		}
		
	}
	 return m;
	
}









